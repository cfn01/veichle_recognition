import os
import PIL
from PIL import Image
import numpy as np
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img

index  = 0
index2 = 0

datagen = ImageDataGenerator(
        rotation_range=40,
        width_shift_range=0.2,
        height_shift_range=0.2,
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True,
        fill_mode='nearest')

#Directory containing images you wish to convert
# input_dir = "/Users/francescoamaddeo/Desktop/mldm-project/front_car_db"
input_dir = "front_car_db"
array_out = np.array([])
directories = os.listdir(input_dir)

for folder in directories[0:500]:
    #Ignoring .DS_Store dir
    if folder == '.DS_Store':
		pass
    else:
        print folder
        folder2 = os.listdir(input_dir + '/' + folder)
        index += 1
        len_images = len(folder2)
        len_images80 = (len_images * 80)/100 #Getting index of image on the 80% mark
        len_images20 = (len_images * 20)/100 #Getting index of image on the 20% mark
		
       	for image in folder2[0:int(len_images80)]:
        	if image == ".DS_Store":
	            pass

    		else:
                    index2 += 1

                    img = load_img(input_dir+"/"+folder+"/"+image)# this is a PIL image
                    x = img_to_array(img)  # this is a Numpy array with shape (3, 150, 150)
                    x = x.reshape((1,) + x.shape)  # this is a Numpy array with shape (1, 3, 150, 150)
                   # array_out = np.append(array_out, x, 0)

                    # the .flow() command below generates batches of randomly transformed images
                    # and saves the results to the `preview/` directory
                    i = 0
                    for batch in datagen.flow(x, batch_size=1, save_to_dir='preview/'+folder, save_prefix = folder, save_format='jpeg'):
                        i += 1
                        if i > 20:
                            break  # otherwise the generator would loop indefinitely

#np.save('files/np_preprocessing.npy', x) #Saving train image arrays
