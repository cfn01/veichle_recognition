La procedura per l'utilizzo del codice messo a disposizione come progetto per l'esame di MLDM si compone di 3 fasi:
1. Esecuzione con python dello script "fase2_rescale"
2. I file prodotti dalla seconda fase vengono posti nella directory "files". E' neccessario spostare i file binari nella directory che contiene lo script "fase3_main" e effuttare una sostituzione "0070" con "0000" nei file "Y_test_fcdb" e "Y_train_fcdb".
3. Esecuzione con python dello script "fase3_main"