\contentsline {chapter}{\numberline {1}Basi del Machine Learning}{1}
\contentsline {section}{\numberline {1.1}Machine Learning}{1}
\contentsline {section}{\numberline {1.2}Learning Algorithms}{1}
\contentsline {section}{\numberline {1.3}Algoritmi per tipologia di apprendimento}{2}
\contentsline {subsection}{\numberline {1.3.1}Apprendimento supervisionato}{2}
\contentsline {subsection}{\numberline {1.3.2}Apprendimento non supervisionato}{2}
\contentsline {subsection}{\numberline {1.3.3}Learning con controllo parziale}{3}
\contentsline {section}{\numberline {1.4} Algoritmi raggruppati per somiglianza}{4}
\contentsline {subsection}{\numberline {1.4.1}Algoritmi di regressione}{4}
\contentsline {subsection}{\numberline {1.4.2}Algoritmi Albero di decisione}{4}
\contentsline {subsection}{\numberline {1.4.3}Algoritmi bayesiani}{5}
\contentsline {subsection}{\numberline {1.4.4}Algoritmi di clustering}{6}
\contentsline {subsection}{\numberline {1.4.5}Association rule learning}{6}
\contentsline {subsection}{\numberline {1.4.6}Artificial Neural Network }{7}
\contentsline {subsection}{\numberline {1.4.7}Deep Learning Algorithms}{7}
\contentsline {chapter}{\numberline {2}Deep Learning}{9}
\contentsline {section}{\numberline {2.1}Introduzione}{9}
\contentsline {section}{\numberline {2.2}Riconoscimento di oggetti attraverso Deep Learning}{10}
\contentsline {section}{\numberline {2.3}Dataset MNIST}{10}
\contentsline {section}{\numberline {2.4}Tunnel Vision}{12}
\contentsline {section}{\numberline {2.5}Circonvoluzione}{13}
\contentsline {chapter}{\numberline {3}Riconoscimento di veicoli}{19}
\contentsline {section}{\numberline {3.1}Raccolta dati}{19}
\contentsline {section}{\numberline {3.2}Keras}{20}
\contentsline {section}{\numberline {3.3}Data pre-processing and data augmentation}{21}
\contentsline {section}{\numberline {3.4}Addestramento delle rete}{24}
\contentsline {section}{\numberline {3.5}Risultati ottenuti}{26}
\contentsline {section}{\numberline {3.6}Support Vector Machine e KNearest}{26}
\contentsline {section}{\numberline {3.7}Risultati ottenuti}{29}
\contentsline {section}{\numberline {3.8}Conclusione}{30}
\contentsline {chapter}{Index}{31}
